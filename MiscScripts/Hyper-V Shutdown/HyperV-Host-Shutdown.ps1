﻿#
# HyperV-Host-Shutdown.ps1
#
#1) Get list of running virtual machines on the host (get-vm)
#2) Foreach VM in list, shutdown virtual machine
#3) Foreach VM in list, check VM status is stopped (also get-vm I believe)
#4) If not stopped, wait x time period and check again
#5) Proceed to host shutdown

# Email Param
$gmailUser = 'someone@gmail.com'
$gmailPwd = ConvertTo-SecureString -String 'somefancypwd' -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ($gmailUser,$gmailPwd)
$param = @{
	SmtpServer = 'smtp.gmail.com'
	Port = 587
	UseSsl = $true
	Credential = $cred
	From = "someone@gmail.com"
	To = "someone@gmail.com"
	Subject = "VM Host Shutdown"
	Body = $body
}

# Loop Exit Criteria
$ShutDown = $false
$ExitLoop = $false
$body = ""

# Source for our event log
New-EventLog –LogName Application –Source “UPS Shutdown Script”

DO
{
	# Get a list of running VM's
	$vms = Get-VM | Where { $_.State –eq ‘Running’ }

	# Determine if we are on battery or not
	# Statuses are as follows:
	# Value Meaning
	# 1 The battery is discharging.
	# 2 The system has access to AC so no battery is being discharged. However, the battery is not necessarily charging.
	# 3 Fully Charged
	# 4 Low
	# 5 Critical
	# 6 Charging
	# 7 Charging and High
	# 8 Charging and Low
	# 9 Charging and Critical
	# 10 Undefined
	# 11 Partially Charged

	$OnBattery = (Get-WmiObject -Class Win32_Battery).BatteryStatus

	# If our battery is low or critical...
	if( ($OnBattery -eq 4) -or ($OnBattery -eq 5) ) 
	{
		$EstRemaining = (Get-WmiObject -Class Win32_Battery).EstimatedChargeRemaining
		$body += "Low battery shutdown of TWIG Hyper-V Host.`n`n Hyper-V Host UPS Battery is at $EstRemaining%.`n`n"
        # Write to our event log
        Write-EventLog –LogName Application –Source “UPS Shutdown Script” –EntryType Information –EventID 1 –Message “Hyper-V Host UPS Battery is at $EstRemaining%. Preparing to shut down.”

		# if we have VMs still running...	
		if ($vms)
		{
			$body += "Saving the following virtual machines before shutdown`n"
			foreach ($machine in $vms) 
			{
				$body += $machine.Name + "`n"
                
                Write-EventLog –LogName Application –Source “UPS Shutdown Script” –EntryType Information –EventID 1 –Message “Scripted shutdown of $machine.”

				# Save the VM
				Save-VM -Name $machine.Name
			}

		}
		
		# Exit the loop
		$ExitLoop = $true

		# Shutdown after exit
		$ShutDown = $true
	}
	# We are no longer on battery
	elseif ($OnBattery -eq 2) 
	{
		$ExitLoop  = $true
	}
	else 
	{
		# Sleep for 20 seconds
		Start-Sleep -Seconds 20
	}
} While (!$ExitLoop)	

# If we need to shutdown, we do so, otherwise we exit the script
if ($ShutDown) 
{
    $now = Get-Date
    $body += "Shutting down at $now"
    Write-EventLog –LogName Application –Source “UPS Shutdown Script” –EntryType Information –EventID 1 –Message “Scripted server shutdown.”
	Send-MailMessage @param  

	# Shut down the computer
	# Stop-Computer
}

