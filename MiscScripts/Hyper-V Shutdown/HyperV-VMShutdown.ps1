﻿#
# HyperV-HostShutdown.ps1
# 
# If we are on UPS battery and the battery percentage is low then we want to shut down the VMs and then power down the host.
#

# Include our mail functions
. "$PSScriptRoot\Functions.ps1"

# Get the time the script started
$StartTime = Get-Date

# Check the source for the event log to make sure it exists before we write to it
if ([System.Diagnostics.EventLog]::SourceExists("UPS Powershell Script") -eq $false)
{
    New-EventLog –LogName Application –Source “UPS Powershell Script”
}

$statusHash=@{
    [int]1 = "Discharging"
    [int]2 = "On A/C"
    [int]3 = "Fully Charged"
    [int]4 = "Low"
    [int]5 = "Critical"
    [int]6 = "Charging"
    [int]7 = "Charging High"
    [int]8 = "Charging Low"
    [int]9 = "Charging Critical"
    [int]10 = "Undefined"
    [int]11 = "Partially Charged"
}

# Get battery details
$Battery = Get-WmiObject -Class Win32_Battery

$body = "The Powershell Shutdown script has been triggered by $($Battery.Name). The script started monitoring the UPS battery at $StartTime. The battery currently has $($Battery.EstimatedChargeRemaining)% remaining. The estimated runtime remaining is $(New-TimeSpan -Minutes $Battery.EstimatedRunTime)."

# Write to the event log
Write-EventLog –LogName Application –Source “UPS Powershell Script” –EntryType Warning –EventID 1 –Message $body
# Send an initial email
Send-GmailAPI "Powershell Shutdown Script Initiated" $body

# This would be perfect but it doesn't run on the server. Likely it's an access rights issue with WMI permissions. https://technet.microsoft.com/en-us/library/cc731011(v=ws.11).aspx
#$(gwmi -Class batterystatus -Namespace root\wmi).PowerOnline

# If we have VMs that we want to shutdown immediatly we'll need a list of names so we can shut them down here.
<# if ($Battery.BatteryStatus -ne 2) 
{
    ("S8", "S6", "S4") | Save-VM
} #>

# Initialize the counter
$Count = 0

# This flag is set to true as long as these VMs are running. As soon as this script has been running for more than 15 minutes we shut down the first VMs and set this value to false.
# This way we skip trying to shut down the vms every time through the loop after 15.
$VMsUp = $true


# Do this as long as the battery is discharging (1).
# While ($Battery.BatteryStatus -eq 1) 

# Do this as long as we are not "On A/C" (not 2). Once we are at 2 ("On A/C") we exit the loop and the script. 
# The only other way to exit is through host shutdown.
While ($Battery.BatteryStatus -ne 2)
{
    # Get elapsed time
    $elapsedTime = $(Get-Date) - $StartTime

    # Check to see the battery status
    $Battery = Get-WmiObject -Class Win32_Battery

    # When $Count is divisble by 5, write status to the event log (approximatly every 5 minutes)
    if ($Count % 5 -eq 0) 
    {
        # Write to the event log
        Write-EventLog –LogName Application –Source “UPS Powershell Script” –EntryType Information –EventID 2 –Message “$env:computername has been running on UPS Battery since $StartTime. The UPS has an estimated $($Battery.EstimatedChargeRemaining)% charge remaining. The estimated runtime remaining is $(New-TimeSpan -Minutes $Battery.EstimatedRunTime). Battery status is $($statushash.Item($($Battery.BatteryStatus -as [int]))) ($($Battery.BatteryStatus)).”
    }
    
    # If we have been running for more than 15 minutes AND the vms468 flag is true, shut down or save VMs but keep the host running
    if ( ( $elapsedTime.TotalSeconds -gt 900 ) -and ( $VMsUp ) )
    {
        $vmssaved = @()

        # List of VMs to save/close
        $vms = ("S8", "S6", "S4")
        foreach ($vm in $vms) 
        {
            # Get the details about the VM
            $vmstats = Get-VM $vm

            # If the VM is running...
            if ($($vmstats.State) -eq "Running") 
            {
                # Save it and write to the log
                Save-Vm $vmstats.Name
                
                # This is an array of VM names that we actually saved
                $vmssaved += $vmstats.Name
            }

        }

        $body = “$env:computername has been running on UPS Battery since $StartTime. The UPS batteries have an estimated $($Battery.EstimatedChargeRemaining)% charge remaining.`r`nThe estimated runtime remaining is $(New-TimeSpan -Minutes $Battery.EstimatedRunTime). Battery status is $($statushash.Item($($Battery.BatteryStatus -as [int]))) ($($Battery.BatteryStatus)). `r`n`r`nThe following Virtual Machines have been saved: $vmssaved.”        
        
        # Write to the event log
        Write-EventLog –LogName Application –Source “UPS Powershell Script” –EntryType Warning –EventID 3 –Message $body
        # Send an email
        Send-GmailAPI "Powershell Shutdown Script VM Shutdown" $body

        # Set the flag here.
        $VMsUp = $false
    }

    # If the battery drops below 30% we need to get the VMs saved and the host powered down.
    if ($Battery.EstimatedChargeRemaining -lt 30) 
    {   
        $body = "$env:computername has been running on battery since $StartTime. The battery is now at $($Battery.EstimatedChargeRemaining)% with an estimated remaining runtime of $(New-TimeSpan -Minutes $Battery.EstimatedRunTime). Battery status is $($statushash.Item($($Battery.BatteryStatus -as [int]))) ($($Battery.BatteryStatus)). `r`nThe host will now shut down.`r`n"

        # Get a list of running VM's
        $vms = Get-VM | Where { $_.State –eq ‘Running’ } | Sort-Object Name -Descending

        # Save any running VMs
        $vms | Save-VM

        if ($vms)        
        {
            $body += "Saving the following virtual machines prior to host shutdown:`n"
            foreach ($machine in $vms) 
            {
                $body += $machine.Name + " `n"
            }
            
        } 
    
        # Write to the event log
        Write-EventLog –LogName Application –Source “UPS Powershell Script” –EntryType Warning –EventID 3 –Message $body
        # Send email 
        Send-GmailAPI "Powershell Shutdown Script Initiated Host Shutdown" $body

        # Pause for just a few seconds
        Start-Sleep -Seconds 5

        # Send email
        Send-GmailAPI "Powershell Shutdown Script Shutdown Complete" "All VMs have been issued the Save command, and host shutdown is being initiated. The host will need to be restarted once utility power is restored.`r`nPowershell monitoring script is ending at $(Get-Date -format G)."

        # Shutdown the host
        Stop-Computer -Confirm:$false -Force
    }
    else 
    {
        # Wait 60 seconds
        Start-Sleep -Seconds 60
    } 
    
    # Increment the counter
    $Count++  
} 