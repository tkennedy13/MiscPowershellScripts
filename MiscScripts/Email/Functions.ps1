﻿# Changelog
#
# v1.0.1
#   Added -UseBasicParsing for Invoke-WebRequest as IE is not available when running as SYSTEM
#   The error was: "The response content cannot be parsed because the Internet Explorer engine is not available, or Internet Explorer's first-launch configuration is not complete."
#   This occurs because the script is running as "SYSTEM" and this user has never logged in to the server and setup IE.

Add-Type -Path "$PSScriptRoot\AE.Net.Mail.dll"
Add-Type -AssemblyName System.IO
Add-Type -AssemblyName System.Text.Encoding

Function Encode-Base64Url([string]$MsgIn) 
{
    $InputBytes = [System.Convert]::ToBase64String([System.Text.Encoding]::UTF8.GetBytes($MsgIn))
    
    # "Url-Safe" base64 encodeing
    $InputBytes = $InputBytes.Replace('+', '-').Replace('/', '_').Replace("=", "")
    
    return $InputBytes
}
Function Send-GmailAPI 
{
    #  From https://gist.github.com/LindaLawton/55115de5e8b366be3969b24884f30a39
    #  Setup:
    #
    #  Step 1: create new project on https://console.developers.google.com.
    #  Step 2: Create oauth credentials type native or other.
    #          Save the client id and secret. 
    #  Step 3: Enable the api you are intersted in accessing.
    #          Look up what scopes you need for accssing this api,
    #  Step 4: Using the client id, and client secret from the 
    #
    #
    # Inital Authenticate:  Authentication must be done the first time via a webpage create the link you will need.  More then one scope can be added simply by seporating them with a comama
    #     Place it in a webbrowser. 
    #
    #    https://accounts.google.com/o/oauth2/auth?client_id={CLIENT ID}&redirect_uri=urn:ietf:wg:oauth:2.0:oob&scope={SCOPES}&response_type=code
    #    Change Scopes to https://www.googleapis.com/auth/gmail.send
    #                     https://www.googleapis.com/auth/gmail.readonly
    #                     https://mail.google.com/
    #
    #    Copy the authencation code and run the following script.  
    #      note: AuthorizationCode can only be used once you will need to save the refresh token returned to you.  
    Param(
        [parameter(Mandatory=$true)]
        [String]$Subject,
        [parameter(Mandatory=$true)]
        [String]$Message,
        [String[]]$ToEmail = @("address3@gmail.com", "address2@gmail.com", "address@yahoo.com"),        
        [String]$FromEmail = "fromsomeone@place.org",
        [String]$ClientID = "Your Client ID",
        [String]$ClientSecret = "Your Client Secret",
        [String]$AuthorizationCode = "Your Authorization Code"
        )

    $RedirectURI = "urn:ietf:wg:oauth:2.0:oob"
    
    # Keep this part, you may need it to get a new refresh token.
    <#$TokenParams = @{
	      client_id=$ClientID;
  	      client_secret=$ClientSecret;
          code=$AuthorizationCode;
	      grant_type='authorization_code';
	      redirect_uri=$RedirectURI;
	    } 

    $Token = Invoke-WebRequest -Uri "https://accounts.google.com/o/oauth2/token" -Method POST -Body $TokenParams | ConvertFrom-Json
        
    # Save this
    $Token.refresh_token 
    $RefreshToken = $Token.refresh_token   #>

    # Use the refresh token to get a new access token
    # The access token is used to access the api by sending the access_token parm with every request. 
    # Access tokens are only valid for an hour, after that you will need to request a new one using your refresh_token
    $RefreshToken = "REFRESH-TOKEN" # Captured from above.

    $RefreshTokenParams = @{
	      client_id=$ClientID;
  	      client_secret=$ClientSecret;
          refresh_token=$RefreshToken;
	      grant_type='refresh_token';
	    }

    $RefreshedToken = Invoke-WebRequest -Uri "https://accounts.google.com/o/oauth2/token" -Method POST -Body $RefreshTokenParams -UseBasicParsing | ConvertFrom-Json

    $AccessToken = $RefreshedToken.access_token

    # Compose and send an email using the access token
    # We are using AE.Net.Mail to create our message. https://github.com/andyedinborough/aenetmail
    $From = New-Object MailAddress($FromEmail)
    #$To = New-Object MailAddress($ToEmail)

    $Msg = New-Object AE.Net.Mail.MailMessage
    $ToEmail | ForEach-Object { 
                                $To = New-Object MailAddress($_) 
                                $Msg.To.Add($To)
                              } 
    $Msg.From = $From
    $Msg.ReplyTo.Add($From) # Important so email doesn't bounce     
    $Msg.Subject = $Subject
    $Msg.Body = $Message
    $Msg.ContentType = "text/html"

    $MsgSW = New-Object System.IO.StringWriter
    $Msg.Save($MsgSW)

    $EncodedEmail = Encode-Base64Url $MsgSW

    # This was taken from https://github.com/thinkAmi/PowerShell_misc/blob/master/gmail_api/gmail_sender.ps1
    $Content = @{ "raw" = $EncodedEmail; } | ConvertTo-Json

    $Result = Invoke-RestMethod -Uri "https://www.googleapis.com/gmail/v1/users/me/messages/send?access_token=$AccessToken" -Method POST -ErrorAction Stop -Body $Content -ContentType "Application/Json"

    return $Result
}
